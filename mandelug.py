#/bin/python

#nos fuimos con esta vaina. 
#buscamos calcular la constante de mandelung. 
# buscamos calcular la constante valiendonos de dos series propuestas
#lo que buscamos en calcular como cuantos terminos necesitamos para asercarnos al valor teorico entonces para cada serie, calculamos el valor de la constante para un cierto numero de terminos de la serie y vamos a graficar  m_aprox/m_teorico vs numero de iteraciones. 
import numpy as np
madelung_teo = 1.74756 # valor teorico de la constante de madelung.
def a(l,m,n):
    b = np.power(-1 , l+m+n+1)
    c = np.sqrt(np.power(l,2) + np.power(m,2)+ np.power(n,2) )
   
    return b/c
def sk(k):
    #ok aqui construyo la funcion
    term1 = 0
    term2 = 0
    term3 = 0
    for m in range(-1*k +1 , k-1):
        for n in range(-1*k+1, k-1):
            term1 += a(k,m,n)
            
    for n in range(-1*k+1, k-1):
        term2 += a(k,k,n) 
    term3 = 8*a(k,k,k)
    tot = 6*term1 + 12*term2 + term3
    return tot
    #print term3
def sk2(k): # terminos 1.6 del documento
    term1 = 0
    term2 = 0
    term3 = 0
    term4 = 0
    if( k < 2):
        c = 6*a(1,0,0) + 12*a(1,1,0) + 8*a(1,1,1)
        return c
    term1 = 6*a(k,0,0) + 12*a(k,k,0) + 8*a(k,k,k)
   
    for m in range(1,k):
        for n in range(1,k):
            term2 += a(k,m,n)
            #print "K:" + str(k)+" indices m, n:" + str(m) +","+ str(n)
            
    for n in range(1,k):        
        term3+= a(k,n,0)
        #print " t3 K:" + str(k) + " indice n :" + str(n)
    for n in range(1,k):
        term4 += a(k,k,n)
    #print "Primer ter" + str(term1)
    #print term2 
    #print term3
    #print term4
    tot = term1 + 24*(term2+term3+term4)
    return tot
def  G2k(k): #funcion G2k
    term1 = 3*a(k,0,0)+ 9*a(k,k,0)+ 7*a(k,k,k)
    term2 = 0
    if ( k == 1 ):
        return 3*a(1,0,0) + 9*a(1,1,0)+ 7*a(1,1,1) #G2
    #k = k-1
    for m in range(1, k):
        for n in range(1,k):
            term2 += a(k,m,n)
    term3 = 0
    for n in range(1,k):
        term3 += a(k,0,n) #cambio 
    term4 = 0
    for n in range(1,k):
        term4 += a(k,k,n)

    tot = term1 + 12*term2 + 12*term3 + 18*term4
    return tot
def G2k_1(k):
    term1 = 3*a(k,0,0) + 3*a(k,k,0)+ a(k,k,k)
    term2 = 0 
    if (k == 1):
        return  3*a(1,0,0)+ 3*a(1,1,0) + a(1,1,1) # G1
    for m in range(1,k):
        for n in range(1,k):
            term2 += a(k,m,n)
    term3 = 0
    for n in range(1,k):
        term3 += a(k,0,n) #cambio
    term4 = 0
    for n in range(1,k):
        term4 += a(k,k,n)
    tot = term1 + 12*term2 + 12*term3 + 6*term4
    return tot
def serie1(m): # m ,  maximo term  de aprox
    suma = 0
    
    for k in range(1,m):
        b=sk2(k)
        suma +=b
        #print "iteracion :" + str(k) + ", made:" + str(suma)
        #print  "termino " + str(k) + " , " + str(b)
    #print "Constante de madelung: " + str(suma)
    return suma
def serie2(m): # 
    G1 = 3*a(1,0,0)+ 3*a(1,1,0) + a(1,1,1)
    #G2 =  3*a(1,0,0) + 9*a(1,1,0)+ 7*a(1,1,1)
    G2 = 0
    suma = G1 + G2
    for k in range(2,m):
        #print "ks : " + str(2*k) + " " + str(2*k-1)
        #Tk = G2k(k) #+ G2k_1(k)
        # digamos T, K = 2
        
        Tk = G2k_1(k) + G2k_1(k-1)
        #suma += Tk
    suma1 = 0
    suma2 = 0
    for k in xrange(1,m):
        suma1 += G2k(k) + G2k_1(k+1) # terminos g2 + g4 + g6 .. 
        
    
   # for k in xrange(2,m):
        #suma2 += G2k_1(k) # g1 + g2 +g3 
    suma = suma1 + suma2 + G1
    return suma
#a(1,1,1)
#print madelung_teo 
k = 100
for i in xrange(1,50):
    # calcular la constante para cada i 
    # formatno de impresion : n alpha1 alpha2
    alpha1 = serie1(i)
    alpha2 = serie2(i)
    if (alpha2 == 0):
        continue
    print  str(i) + " "+ str(alpha1) + " "+ str(alpha2) + " "  + str(alpha1/alpha2) 
#serie1(20)
#serie2(20)



